#!/usr/bin/env bash
set -ex

# Login into the registry
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

[ -z "$CI_COMMIT_TAG" ] && VERSION_NAME="$CI_COMMIT_REF_NAME" || VERSION_NAME="$CI_COMMIT_TAG"
VERSION_COMMIT_HASH=$CI_COMMIT_SHORT_SHA

# Pull the cache images to speedup the build
docker pull $CACHE_IMAGE_TAG_BACKEND || true
docker pull $CACHE_IMAGE_TAG_FRONTEND || true
docker pull $CACHE_IMAGE_TAG_NGINX || true



# Build the server image
docker build \
  --cache-from $CACHE_IMAGE_TAG_BACKEND \
  -t $IMAGE_TAG_BACKEND \
  -f .docker/backend/Dockerfile \
  ./packages/backend

docker build \
    --cache-from $IMAGE_TAG_FRONTEND\
    -t $IMAGE_TAG_FRONTEND \
    -f .docker/nginx/Dockerfile_builder \
    ./packages/frontend

docker build \
    --cache-from $IMAGE_TAG_NGINX\
    -t $IMAGE_TAG_NGINX\
    -f .docker/nginx/Dockerfile \
    --build-arg builder="$IMAGE_TAG_FRONTEND" \
    ./.docker/nginx



# Push the built images to the registry
docker push $IMAGE_TAG_BACKEND
docker push $IMAGE_TAG_FRONTEND
docker push $IMAGE_TAG_NGINX

