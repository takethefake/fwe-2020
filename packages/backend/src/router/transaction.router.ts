import { Router } from 'express';

import {
  createTransaction,
  deleteTransaction,
  getTransaction,
  getTransactions,
  patchTransaction,
} from '../controller/transaction.controller';

export const transactionRouter = Router({ mergeParams: true });

transactionRouter.get('/', getTransactions);
transactionRouter.post('/', createTransaction);
transactionRouter.get('/:transactionId', getTransaction);
transactionRouter.delete('/:transactionId', deleteTransaction);
transactionRouter.patch('/:transactionId', patchTransaction);
