import { Router } from 'express';

import { getTags } from '../controller/tag.controller';

export const tagRouter = Router({ mergeParams: true });
tagRouter.get('/', getTags);
