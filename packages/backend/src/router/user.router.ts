import { Router } from 'express';

import { getUsersFromExternalApi, loginUser, registerUser } from '../controller/user.controller';
import { Authentication } from '../middleware/authentication';

export const userRouter = Router({ mergeParams: true });

userRouter.get('/', Authentication.verifyAccess, getUsersFromExternalApi);
userRouter.post('/', registerUser);
userRouter.post('/token', loginUser);
