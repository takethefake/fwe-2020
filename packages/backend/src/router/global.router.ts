import { Request, Response, Router } from 'express';

import { Authentication } from '../middleware/authentication';
import { tagRouter } from './tag.router';
import { transactionRouter } from './transaction.router';
import { userRouter } from './user.router';

export const globalRouter = Router({ mergeParams: true });

globalRouter.get('/', async (_: Request, res: Response) => {
  res.send({ message: 'Hello api' });
});

globalRouter.use('/tag', Authentication.verifyAccess, tagRouter);
globalRouter.use('/transaction', Authentication.verifyAccess, transactionRouter);
globalRouter.use('/user', userRouter);
