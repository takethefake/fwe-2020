import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Tag } from '../entity/Tag';

export const getTags = async (_: Request, res: Response) => {
  const tagRepository = await getRepository(Tag);
  const tags = await tagRepository.find();
  res.send({
    data: tags,
  });
};
