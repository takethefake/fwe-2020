import { Request, Response } from 'express';
import fetch from 'node-fetch';
import { getRepository } from 'typeorm';

import { User } from '../entity/User';
import { Authentication } from '../middleware/authentication';
import { validate } from 'class-validator';

export const getUsersFromExternalApi = async (_: Request, res: Response) => {
  const response = await fetch('https://jsonplaceholder.typicode.com/users', {
    method: 'GET',
  });
  const users = await response.json();
  res.send({
    data: users.map((user: any) => {
      return {
        email: user.email,
        id: user.id,
        name: user.name,
      };
    }),
  });
};

export const registerUser = async (req: Request, res: Response) => {
  const { email, name, password } = req.body;

  const userRepository = await getRepository(User);

  // Check if user exists
  const user = await userRepository.findOne({
    where: {
      email,
    },
  });

  if (user) {
    return res.status(400).send({
      status: 'bad_request',
    });
  }

  // Generate hashed password
  const hashedPassword: string = await Authentication.hashPassword(password);

  const newUser = new User();
  newUser.email = email;
  newUser.name = name;
  newUser.password = hashedPassword;

  const errors = await validate(newUser);
  if (errors.length > 0) {
    return res.status(400).send({ status: 'bad_request' });
  }

  const createdUser = await userRepository.save(newUser);
  delete createdUser.password;

  return res.send({
    data: createdUser,
  });
};

export const loginUser = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const userRepository = await getRepository(User);
  // Check if user exists
  const user = await userRepository.findOne({
    select: ['password', 'email', 'name', 'id'],
    where: {
      email,
    },
  });

  if (!user) {
    return res.status(401).send({ status: 'unauthorized' });
  }

  const matchingPasswords: boolean = await Authentication.comparePasswordWithHash(password, user.password);
  if (!matchingPasswords) {
    return res.status(401).send({ status: 'unauthorized' });
  }

  const token: string = await Authentication.generateToken({
    email: user.email,
    id: user.id,
    name: user.name,
  });

  return res.send({
    data: token,
  });
};
