import { Request, Response } from "express";
import {
  getRepository,
  Repository,
  EntityManager,
  getConnection,
  QueryRunner,
} from "typeorm";

import { Transaction } from "../entity/Transaction";
import { Tag } from "../entity/Tag";
import { User } from "../entity/User";
import { validate } from "class-validator";

export const getTransactions = async (req: Request, res: Response) => {
  const transactionRepository: Repository<Transaction> = getRepository(
    Transaction
  );

  const token = req.token;
  if (token === null) {
    res.status(400).send({ message: "no-token" });
    return;
  }
  const transactions: Transaction[] = await transactionRepository
    .createQueryBuilder("transaction")
    .leftJoinAndSelect("transaction.tags", "tags")
    .leftJoin("transaction.user", "user")
    .where("user.id=:id", { id: req.token!.id })
    .getMany();
  res.send({ data: transactions });
};

/** We are doing a upsert via code here */
const checkExisting = async (tagName: string, manager: EntityManager) => {
  const existingTag = await manager.getRepository(Tag).findOne({
    where: { label: tagName },
  });
  const enTag = new Tag();
  enTag.label = tagName;

  return existingTag || manager.save(enTag);
};

const createTags = async (tags: Tag[], queryRunner: QueryRunner) => {
  const promiseTags = await Promise.all(
    tags.reduce<Promise<Tag | undefined>[]>((prev, tag) => {
      if (tag.id) {
        prev.push(queryRunner.manager.getRepository(Tag).findOne(tag.id));
        return prev;
      }
      if (tag.label) {
        prev.push(checkExisting(tag.label, queryRunner.manager));
        return prev;
      }
      return prev;
    }, [])
  );
  return promiseTags.filter((tag) => tag !== undefined) as Tag[];
};

export const createTransaction = async (req: Request, res: Response) => {
  const { name, value, description, type, tags } = req.body;
  const typedTags = tags as Tag[];
  let dbTags: Tag[] = [];

  const connection = getConnection();
  const queryRunner = connection.createQueryRunner();

  await queryRunner.startTransaction();

  try {
    if (tags) {
      dbTags = await createTags(typedTags, queryRunner);
    }
    /** done with cascading tags via code */

    const currentUser = await queryRunner.manager
      .getRepository(User)
      .findOneOrFail(req.token!.id);
    const transaction = new Transaction();
    transaction.name = name;
    transaction.value = value;
    transaction.description = description;
    transaction.type = type;
    transaction.user = currentUser || null;
    transaction.tags = dbTags;

    const errors = await validate(transaction);
    if (errors.length > 0) {
      res.status(400).send({ errors });
      return;
    }

    const transactionRepository: Repository<Transaction> = queryRunner.manager.getRepository(
      Transaction
    );
    const createdTransaction: Transaction = await transactionRepository.save(
      transaction
    );
    await queryRunner.commitTransaction();

    res.send({ data: createdTransaction });
  } catch (e) {
    await queryRunner.rollbackTransaction();
    res.status(500).send(JSON.stringify(e));
  } finally {
    await queryRunner.release();
  }
};

export const getTransaction = async (req: Request, res: Response) => {
  const transactionId = req.params.transactionId;
  const transactionRepository = await getRepository(Transaction);

  try {
    const transaction = await transactionRepository.findOneOrFail(
      transactionId
    );
    res.send({
      data: transaction,
    });
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

export const deleteTransaction = async (req: Request, res: Response) => {
  const transactionId = req.params.transactionId;
  const transactionRepository = await getRepository(Transaction);

  try {
    const transaction = await transactionRepository.findOneOrFail(
      transactionId
    );
    await transactionRepository.remove(transaction);
    res.send({});
  } catch (e) {
    res.status(404).send({
      status: "not_found",
    });
  }
};

export const patchTransaction = async (req: Request, res: Response) => {
  const transactionId = req.params.transactionId;

  const { name, value, description, type, tags } = req.body;
  const typedTags = tags as Tag[];
  let dbTags: Tag[] = [];

  const transactionRepository = await getRepository(Transaction);
  const transaction = await transactionRepository.findOneOrFail(transactionId);

  const connection = getConnection();
  const queryRunner = connection.createQueryRunner();

  try {
    await queryRunner.startTransaction();
    if (tags) {
      dbTags = await createTags(typedTags, queryRunner);
    }
    /** done with cascading tags via code */

    transaction.name = name;
    transaction.value = value;
    transaction.description = description;
    transaction.type = type;
    transaction.tags = dbTags;

    const errors = await validate(transaction);
    if (errors.length > 0) {
      res.status(400).send({ errors });
      return;
    }

    const transactionRepository: Repository<Transaction> = queryRunner.manager.getRepository(
      Transaction
    );
    const updatedTransaction: Transaction = await transactionRepository.save(
      transaction
    );
    await queryRunner.commitTransaction();

    res.send({ data: updatedTransaction });
  } catch (e) {
    await queryRunner.rollbackTransaction();
    res.status(500).send(JSON.stringify(e));
  } finally {
    await queryRunner.release();
  }
};
