import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  Entity,
  ManyToMany,
} from "typeorm";
import { Transaction } from "./Transaction";

@Entity()
export class Tag {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column()
  label: string;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @ManyToMany(() => Transaction, (transaction) => transaction.tags)
  transactions: Transaction[];
}
