import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from "typeorm";
import { Tag } from "./Tag";
import { User } from "./User";
import { IsString } from "class-validator";

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  @IsString()
  name: string;

  @Column({ type: "text", nullable: true })
  description: string;

  @Column()
  value: string;

  @Column({
    default: "expense",
  })
  type: "income" | "expense";

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @ManyToMany(() => Tag, (tag) => tag.transactions, {
    cascade: true,
    eager: true,
  })
  @JoinTable({ name: "transaction_tags" })
  tags: Tag[];

  @ManyToOne(() => User, (user) => user.transactions)
  user: User;
}
