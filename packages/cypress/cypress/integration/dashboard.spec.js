import { userBuilder } from "../builder/User";
import { transactionBuilder } from "../builder/Transaction";

describe("DashboardPage", () => {
  it("can create a new transaction", () => {
    cy.loginNewUser({}).then((user) => {
      const transaction = transactionBuilder({})();
      cy.visit("/dashboard");
      cy.screenshot();
      cy.findByTestId(/add-transaction/i).click();
      cy.findByLabelText(/name/i).type(transaction.name);
      cy.findByLabelText(/value/i).type(transaction.value);
      cy.findByLabelText(/description/i).type(transaction.description);
      cy.findByLabelText(/tags/i).type("TestTag {enter}", { force: true });
      cy.findByText("Add Transaction").click();
      cy.findByTestId("transaction-item").should("have.length", 1);
      cy.screenshot();
    });
  });
  it("can logout", () => {
    cy.loginNewUser({}).then((user) => {
      cy.visit("/dashboard");
      cy.findByText(/logout/i).click();
      cy.url().should("contain", "/login");
    });
  });
});
