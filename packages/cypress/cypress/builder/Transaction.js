import { build, fake } from "test-data-bot";

export const transactionBuilder = ({ expense }) =>
  build("Transaction").fields({
    name: fake(f => f.lorem.words()),
    value: fake(f => f.finance.amount(10, 200, 2)),
    description: fake(f => f.lorem.words()),
    expense: fake(f => f.random.boolean())
  });
