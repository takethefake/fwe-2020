import React, { useContext } from 'react';
// tslint:disable-next-line: no-submodule-imports
import styled, { css } from 'styled-components/macro';
import { Logo } from './Logo';
import { Link } from 'react-router-dom';
import { authContext } from '../contexts/AuthenticationContext';

const TextButton = styled.button`
  all: unset;
  color: ${(props) => props.theme.colors.primary};
  pointer: click;
`;

const headerHeight = '85px';
const footerHeight = '50px';

export const MaxWidthCSS = css`
  max-width: 860px;
  margin: auto;
`;
const Header = styled.header`
  height: ${headerHeight};
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 25px;
`;

const Main = styled.main`
  min-height: calc(100vh - ${headerHeight} - ${footerHeight});
  padding: 0 25px;
  ${MaxWidthCSS}
`;

const Footer = styled.footer`
  height: ${footerHeight};
  padding: 0 25px;
  ${MaxWidthCSS};
`;

const NavigationList = styled.ul`
  list-style: none;
`;
const NavigationItem = styled(Link)`
  color: ${(props) => props.theme.colors.primary};
  text-decoration: none;
  margin-right: 15px;
`;

export const Layout: React.FC = ({ children }) => {
  const {
    actions: { logout },
  } = useContext(authContext);
  const onLogout = () => {
    logout();
  };
  return (
    <>
      <Header>
        <Logo />
        <NavigationList>
          <NavigationItem to="/dashboard">Home</NavigationItem>
          <TextButton onClick={onLogout}>Logout</TextButton>
        </NavigationList>
      </Header>
      <Main>{children}</Main>
      <Footer>© 2020 AWD Lecture</Footer>
    </>
  );
};
