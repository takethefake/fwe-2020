import React from 'react';

export const Logo = () => {
  return (
    <div
      css={`
        font-size: 25px;
        letter-spacing: 2.3px;
        flex: 1;
      `}
    >
      <span
        css={`
          text-decoration: underline overline;
        `}
      >
        AWD
      </span>
      20
    </div>
  );
};
