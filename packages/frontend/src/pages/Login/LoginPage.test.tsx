import React from 'react';
import { render, fireEvent } from '../../utils/tests';
import { authContext, initialAuthContext } from '../../contexts/AuthenticationContext';
import { LoginPage } from './LoginPage';
import { BrowserRouter } from 'react-router-dom';

describe('LoginPage', () => {
  it('is able to login', () => {
    const loginMock = jest.fn();
    const { getByLabelText, getByText } = render(
      <BrowserRouter>
        <authContext.Provider
          value={{ ...initialAuthContext, actions: { ...initialAuthContext.actions, login: loginMock } }}
        >
          <LoginPage />
        </authContext.Provider>
        ,
      </BrowserRouter>,
    );
    const email = getByLabelText(/email/i);
    const password = getByLabelText(/password/i);
    const submitButton = getByText(/log in/i);

    const testUser = {
      email: 'test',
      password: '123456',
    };

    fireEvent.change(email, { target: { value: testUser.email } });
    fireEvent.change(password, { target: { value: testUser.password } });
    fireEvent.click(submitButton);

    expect(loginMock).toHaveBeenCalledTimes(1);
    expect(loginMock).toHaveBeenCalledWith(testUser);
  });
});
