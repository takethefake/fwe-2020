import React, { useEffect, useContext, useState } from 'react';
// tslint:disable-next-line: no-submodule-imports
import styled from 'styled-components/macro';
import { Transaction, TransactionType, TransactionList, TransactionItem } from './components/TransactionList';
import { AddButton } from './components/AddButton';
import { Layout } from '../../components/Layout';
import { authContext } from '../../contexts/AuthenticationContext';
import { AddTransactionForm } from './components/AddTransactionFrom';
import { Modal } from '../../components/Modal';
import { EditTransactionForm } from './components/EditTransactionForm';

const CurrentBalance = styled.div`
  font-size: 14px;
  color: ${(props) => props.theme.colors.secondaryFontColor};
`;

export const DashboardPage = () => {
  const { token } = useContext(authContext);
  const [addTransactionVisible, setAddTransactionVisible] = useState(false);
  const [editTransaction, setEditTransaction] = useState<Transaction | null>(null);
  const [transactions, setTransactions] = useState<Transaction[]>([]);

  const fetchTransactions = async () => {
    const transactionRequest = await fetch('/api/transaction', {
      headers: { 'content-type': 'application/json', authorization: token! },
    });
    if (transactionRequest.status === 200) {
      const transactionJSON = await transactionRequest.json();
      setTransactions(transactionJSON.data);
    }
  };

  useEffect(() => {
    fetchTransactions();
  }, []);
  const summedTransactions = transactions.reduce((prev, cur) => {
    const transactionValue = cur.type === TransactionType.EXPENSE ? parseFloat(cur.value) * -1 : parseFloat(cur.value);
    return transactionValue + prev;
  }, 0);

  return (
    <Layout>
      <div
        css={`
          display: flex;
          flex-direction: row;
          width: 100%;
        `}
      >
        <div>
          <h2>Home</h2>
          <p
            css={`
              font-size: 36px;
              margin: 0;
            `}
          >
            €{summedTransactions.toFixed(2)}
          </p>
          <CurrentBalance>Current Balance</CurrentBalance>
        </div>
        <div
          css={`
            flex: 1;
            justify-content: flex-end;
            display: flex;
            align-items: top;
          `}
        >
          <AddButton
            data-testid="add-transaction-button"
            onClick={() => {
              if (!editTransaction) {
                setAddTransactionVisible(true);
              }
            }}
          />
        </div>
      </div>
      {addTransactionVisible && (
        <Modal
          title="Add a new Transaction"
          onCancel={() => {
            setAddTransactionVisible(false);
          }}
        >
          <AddTransactionForm
            afterSubmit={() => {
              setAddTransactionVisible(false);
              fetchTransactions();
            }}
          />
        </Modal>
      )}
      {editTransaction && (
        <Modal
          title="Edit Transaction"
          onCancel={() => {
            setEditTransaction(null);
          }}
        >
          <EditTransactionForm
            afterSubmit={() => {
              setEditTransaction(null);
              fetchTransactions();
            }}
            transaction={editTransaction}
          />
        </Modal>
      )}

      <TransactionList>
        {transactions.map((transaction) => (
          <TransactionItem
            key={transaction.id}
            onClick={() => {
              if (!addTransactionVisible) {
                setEditTransaction(transaction);
              }
            }}
            transaction={transaction}
          />
        ))}
      </TransactionList>
    </Layout>
  );
};
