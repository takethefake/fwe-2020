import React from 'react';
import { render, act, fireEvent, getByText, waitFor } from '@testing-library/react';
import { DashboardPage } from './DashboardPage';
import { BrowserRouter } from 'react-router-dom';
import { FetchMock } from 'jest-fetch-mock';
import { TransactionType } from './components/TransactionList';
import { AuthProvider } from '../../contexts/AuthenticationContext';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';

describe('DashboardPage', () => {
  beforeEach(() => {
    (fetch as FetchMock).resetMocks();
  });
  it('adds an Item after Create', async () => {
    const transactionInitialFetchResponse = {
      data: [],
      status: 'ok',
    };

    const transactionPostResponse = {
      data: {
        createdAt: new Date('2019-01-01'),
        description: 'Testdescription',
        id: '1',
        name: 'TestName',
        tags: [],
        type: TransactionType.EXPENSE,
        updatedAt: new Date('2019-01-01'),
        value: 2.5,
      },
      status: 'ok',
    };

    const transactionResponse = {
      data: [
        {
          createdAt: new Date('2019-01-01'),
          description: 'Testdescription',
          id: '1',
          name: 'TestName',
          tags: [],
          type: TransactionType.EXPENSE,
          updatedAt: new Date('2019-01-01'),
          value: 2.5,
        },
      ],
      status: 'ok',
    };
    (fetch as FetchMock)
      .once(JSON.stringify(transactionInitialFetchResponse))
      .once(JSON.stringify(transactionPostResponse))
      .once(JSON.stringify(transactionResponse));
    const { getByLabelText: getByLabelTextContainer, getByTestId, findAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <AuthProvider>
            <DashboardPage />
          </AuthProvider>
        </BrowserRouter>
      </ThemeProvider>,
    );

    const createTransaction = getByTestId('add-transaction-button');

    await act(async () => {
      fireEvent.click(createTransaction);
    });
    await waitFor(() => {
      expect(getByTestId('add-transaction-form')).toBeInTheDocument();
    });
    const transactionForm = getByTestId('add-transaction-form');

    const name = getByLabelTextContainer(/name/i);
    const value = getByLabelTextContainer(/value/i);
    const description = getByLabelTextContainer(/description/i);

    fireEvent.change(name, {
      target: { value: transactionResponse.data[0].name },
    });
    fireEvent.change(value, {
      target: { value: transactionResponse.data[0].value },
    });
    fireEvent.change(description, {
      target: { value: transactionResponse.data[0].description },
    });

    const submit = getByText(transactionForm, /add transaction/i);
    fireEvent.click(submit);

    await findAllByTestId('transaction-item');
    expect((await findAllByTestId('transaction-item')).length).toBe(1);
  });
});
