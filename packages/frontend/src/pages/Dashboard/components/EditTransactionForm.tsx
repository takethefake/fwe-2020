import React, { useContext, useState, ChangeEvent } from 'react';
import { authContext } from '../../../contexts/AuthenticationContext';
import { tagContext } from '../../../contexts/TagContext';
import { Input } from '../../../components/Input/Input';
import { Button, DangerButton } from '../../../components/Button';
import { TransactionType, Transaction } from './TransactionList';
import { SelectInput, Option } from '../../../components/SelectInput';

interface EditTransactionFormState {
  name: string;
  description: string;
  value: string;
  tags: Option[];
}

export const EditTransactionForm: React.FC<{
  afterSubmit: () => void;
  transaction: Transaction;
}> = ({ afterSubmit, transaction }) => {
  const { token } = useContext(authContext);
  const {
    tags,
    actions: { refetch: refetchTags },
  } = useContext(tagContext);
  const [values, setValues] = useState<EditTransactionFormState>(transaction);
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const transactionType = parseFloat(values.value) < 0 ? TransactionType.EXPENSE : TransactionType.INCOME;
    await fetch(`/api/transaction/${transaction.id}`, {
      body: JSON.stringify({
        ...values,
        type: transactionType,
        value: Math.abs(parseFloat(values.value)).toString(),
      }),
      headers: { 'Content-Type': 'application/json', Authorization: token! },
      method: 'PATCH',
    });
    await refetchTags();
    afterSubmit();
  };

  const deleteTransaction = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    await fetch(`/api/transaction/${transaction.id}`, {
      headers: { 'Content-Type': 'application/json', Authorization: token! },
      method: 'DELETE',
    });
    afterSubmit();
  };

  return (
    <form onSubmit={onSubmitForm} data-testid="edit-transaction-form">
      <Input name="name" type="text" label="Name" onChange={fieldDidChange} required value={values.name} />
      <Input
        name="description"
        label="Description"
        type="text"
        onChange={fieldDidChange}
        required
        value={values.description}
      />
      <Input
        name="value"
        label="Value"
        type="number"
        step="0.01"
        onChange={fieldDidChange}
        required
        value={values.value}
      />

      <SelectInput
        label="Tags"
        options={tags}
        initialState={{ inputValue: '', selectedOptions: values.tags }}
        onChangeSelectedOptions={(options) => {
          setValues({ ...values, tags: options });
        }}
      />
      <Button type="submit">Edit Transaction</Button>
      <DangerButton onClick={deleteTransaction}>Delete Transaction</DangerButton>
    </form>
  );
};
