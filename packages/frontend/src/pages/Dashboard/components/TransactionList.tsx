import React from 'react';
import styled from 'styled-components';

export enum TransactionType {
  INCOME = 'income',
  EXPENSE = 'expense',
}

export type Tag = {
  id: string;
  label: string;
  createdAt: Date;
  updatedAt: Date;
};

export type Transaction = {
  id: string;
  name: string;
  description: string;
  value: string;
  createdAt: Date;
  updatedAt: Date;
  tags: Tag[];
  type: TransactionType;
};

const TagList = styled.ul`
  list-style: none;
  flex-grow: 1;
  font-size: 0.8rem;

  align-self: flex-end;
  display: flex;
  & > li {
    margin-right: 0.5rem;
    padding: 0.125rem;
    border-radius: 0.25rem;
    background-color: ${(props) => props.theme.colors.primary};
    display: block;
    color: #333;
  }
`;

const TransactionFlex = styled.div`
  display: flex;
  align-items: center;
`;

export const TransactionHighlight = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  display: none;
  width: 4px;
  background-color: ${(props) => props.theme.colors.primary};
`;

export const TransactionItemStyle = styled.div`
  margin: 0;
  min-height: 3rem;
  position: relative;
  padding: 0.7rem 2rem;
  &:hover {
    ${TransactionHighlight} {
      display: block;
    }
  }
`;
export const TransactionList = styled.ul`
  list-style: none;
  box-shadow: 0 0.125em 0.25em 0 ${(props) => props.theme.colors.shadowColor};
  width: 100%;
  padding: 0;
  border-radius: 0.5rem;
  background-color: ${(props) => props.theme.colors.listBackgroundColor};
  ${TransactionItemStyle} {
    border-bottom: 1px ${(props) => props.theme.colors.shadowColor} solid;
    &:last-of-type {
      border-bottom: 0;
    }
  }
`;

export const TransactionTitle = styled.p`
  font-size: 1.1rem;
  font-weight: 500;
  margin: 0;
`;

export const TransactionDescription = styled.p`
  font-size: 0.8rem;
  margin: 0;
`;
export const TransactionDate = styled.p`
  margin: 0;
  font-size: 0.8rem;
  color: ${(props) => props.theme.colors.secondaryFontColor};
`;
export const TransactionValue = styled.span`
  white-space: nowrap;
`;
export type TransactionItemProps = {
  transaction: Transaction;
  onClick?: (transaction: Transaction) => void;
};

export const TransactionItem: React.FC<TransactionItemProps> = ({ transaction, onClick = () => undefined }) => {
  const { name, description, createdAt, tags, value, type } = transaction;
  return (
    <TransactionItemStyle
      data-testid="transaction-item"
      onClick={() => {
        onClick(transaction);
      }}
    >
      <TransactionHighlight />
      <TransactionFlex>
        <div>
          <TransactionTitle>{name}</TransactionTitle>
          <TransactionDescription>{description}</TransactionDescription>
          <TransactionDate>{createdAt && createdAt.toLocaleString()}</TransactionDate>
        </div>
        <TagList>
          {tags &&
            tags.map((tag: Tag) => {
              return <li key={tag.id}>{tag.label}</li>;
            })}
        </TagList>

        <TransactionValue>
          {type === TransactionType.EXPENSE ? '-' : ''}€{parseFloat(value).toFixed(2)}
        </TransactionValue>
      </TransactionFlex>
    </TransactionItemStyle>
  );
};
