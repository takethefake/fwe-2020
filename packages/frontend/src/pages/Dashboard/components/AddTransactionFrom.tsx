import React, { useContext, useState, ChangeEvent } from 'react';
import { authContext } from '../../../contexts/AuthenticationContext';
import { tagContext } from '../../../contexts/TagContext';
import { Input } from '../../../components/Input/Input';
import { Button } from '../../../components/Button';
import { TransactionType } from './TransactionList';
import { SelectInput, Option } from '../../../components/SelectInput';

export const AddTransactionForm: React.FC<{ afterSubmit: () => void }> = ({ afterSubmit }) => {
  const { token } = useContext(authContext);
  const {
    tags,
    actions: { refetch: refetchTags },
  } = useContext(tagContext);
  const [values, setValues] = useState({
    description: '',
    name: '',
    tags: [] as Option[],
    value: '',
  });
  const fieldDidChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const onSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const transactionType = parseFloat(values.value) < 0 ? TransactionType.EXPENSE : TransactionType.INCOME;
    await fetch('/api/transaction', {
      body: JSON.stringify({
        ...values,
        type: transactionType,
        value: Math.abs(parseFloat(values.value)).toString(),
      }),
      headers: { 'Content-Type': 'application/json', Authorization: token! },
      method: 'POST',
    });
    await refetchTags();
    afterSubmit();
  };
  return (
    <form onSubmit={onSubmitForm} data-testid="add-transaction-form">
      <Input name="name" type="text" label="Name" onChange={fieldDidChange} required value={values.name} />
      <Input
        name="description"
        label="Description"
        type="text"
        onChange={fieldDidChange}
        required
        value={values.description}
      />
      <Input
        name="value"
        label="Value"
        type="number"
        step="0.01"
        onChange={fieldDidChange}
        required
        value={values.value}
      />

      <SelectInput
        label="Tags"
        options={tags}
        onChangeSelectedOptions={(options) => {
          setValues({ ...values, tags: options });
        }}
      />
      <Button type="submit">Add Transaction</Button>
    </form>
  );
};
