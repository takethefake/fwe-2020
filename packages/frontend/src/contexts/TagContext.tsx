import React, { useState, useContext, useEffect } from 'react';
import { Option } from '../components/SelectInput';
import { authContext } from './AuthenticationContext';

interface TagContext {
  tags: Option[];
  actions: {
    refetch: () => Promise<void>;
  };
}

const initialContext = {
  actions: {
    refetch: async () => undefined,
  },
  tags: [] as Option[],
};

export const tagContext = React.createContext<TagContext>(initialContext);

export const TagProvider: React.FC = ({ children }) => {
  const [tags, setTags] = useState<Option[]>([]);
  const { token } = useContext(authContext);

  const refetch = async () => {
    const tagRequest = await fetch('/api/tag', {
      headers: {
        authorization: token || '',
        'content-type': 'application/json',
      },
    });
    const tagJson = await tagRequest.json();

    setTags(tagJson.data as Option[]);
  };

  useEffect(() => {
    (async () => {
      await refetch();
    })();
  }, []);

  return (
    <tagContext.Provider
      value={{
        actions: {
          refetch,
        },
        tags,
      }}
    >
      {children}
    </tagContext.Provider>
  );
};
