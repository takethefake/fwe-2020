import React from 'react';
// tslint:disable-next-line: no-submodule-imports
import { FetchMock } from 'jest-fetch-mock/types';
import { LoginResponse, LoginOptions, AuthProvider, authContext } from './AuthenticationContext';
import { render, act, fireEvent } from '@testing-library/react';

describe('AuthenticationContext', () => {
  beforeEach(() => {
    (fetch as FetchMock).resetMocks();
  });
  it('should set request on login', async () => {
    const loginResponse: LoginResponse = {
      data: 'randooooom Token',
      status: 'ok',
    };
    const loginRequest: LoginOptions = {
      email: 'test@test.de',
      password: 'password',
    };
    (fetch as FetchMock).mockResponseOnce(JSON.stringify(loginResponse));

    const { getByText } = render(
      <AuthProvider>
        <authContext.Consumer>
          {({ actions: { login } }) => {
            const loginFn = () => login(loginRequest);
            return <button onClick={loginFn}>login</button>;
          }}
        </authContext.Consumer>
      </AuthProvider>,
    );
    const submit = getByText(/login/i);
    await act(async () => {
      fireEvent.click(submit);
    });
    expect((fetch as FetchMock).mock.calls.length).toBe(1);
    expect((fetch as FetchMock).mock.calls[0][0]).toBe('/api/user/token');
    expect(((fetch as FetchMock).mock.calls[0][1] as RequestInit).method).toBe('POST');
    expect(((fetch as FetchMock).mock.calls[0][1] as RequestInit).body).toBe(JSON.stringify(loginRequest));
    expect(((fetch as FetchMock).mock.calls[0][1] as RequestInit).body).toBe(JSON.stringify(loginRequest));
  });
});
