import React, { useEffect, useContext } from 'react';
import { GlobalStyle } from './components/GlobalStyle';
import { ThemeProvider } from 'styled-components';
import { theme } from './theme';
import { DashboardPage } from './pages/Dashboard/DashboardPage';

import { LoginPage } from './pages/Login/LoginPage';
import { AuthProvider, authContext } from './contexts/AuthenticationContext';
import { RegisterPage } from './pages/Register/RegisterPage';
import { BrowserRouter, Switch, Route, Redirect, RouteProps } from 'react-router-dom';
import { TagProvider } from './contexts/TagContext';

export const BasePage = () => {
  const { token } = useContext(authContext);
  if (token) {
    return <Redirect to="/dashboard" />;
  }
  return <Redirect to="/login" />;
};

const UnauthenticatedRoute: React.FC<RouteProps> = ({ children, ...routeProps }) => {
  const { token } = useContext(authContext);
  if (token === null) {
    return <Route {...routeProps} />;
  }
  return <Redirect to="/" />;
};

const AuthenticatedRoute: React.FC<RouteProps> = ({ children, ...routeProps }) => {
  const {
    token,
    actions: { getTokenData, logout },
  } = useContext(authContext);
  if (token !== null) {
    const tokenData = getTokenData();
    if (tokenData !== null) {
      const { exp } = tokenData;
      if (parseInt(exp, 10) * 1000 > Date.now()) {
        return <Route {...routeProps} />;
      }
      logout();
    }
  }
  return <Redirect to="/" />;
};

export const App = () => {
  useEffect(() => {
    (async () => {
      const helloRequest = await fetch('/api');
      const helloJson = await helloRequest.json();
      console.log(helloJson);
    })();
  });

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <AuthProvider>
          <TagProvider>
            <GlobalStyle />
            <Switch>
              <UnauthenticatedRoute exact path="/login" component={LoginPage} />

              <UnauthenticatedRoute exact path="/register" component={RegisterPage} />
              <AuthenticatedRoute exact path="/dashboard" component={DashboardPage} />
              <Route path="/" component={BasePage} />
            </Switch>
          </TagProvider>
        </AuthProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
};
